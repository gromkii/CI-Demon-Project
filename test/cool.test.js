var mocha   = require('mocha'),
    expect  = require('chai').expect
    app     = require('../app');

describe('CoolStuff', () => {
  it('should return a string', () => {
    expect(app.coolFunction()).to.eq('Check out this cool function.');
  });
  it('should return a number', () => {
    expect(app.coolNumber(6)).to.eq(6);
  });
});
